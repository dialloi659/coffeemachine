package com.doditech.controllers;

import com.doditech.models.Drink;
import com.doditech.models.MenuOperation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ControlPanelController {
    private boolean twoCups;
    private ArrayList<Drink> drinkList;
    private final List<String> drinkNameList = Arrays.asList("Cappuccino", "Espresso", "Moccachino", "Lacte");
    public final int validate = 8;
    private final int back = 9;
    private Drink selectedDrink;
    private final MenuOperation menuOperation;
    //------------------------------------------------------------------------------------------------------------------
    public ControlPanelController(){
        this.menuOperation = new MenuOperation();
        this.initDrinks();
        this.setSelectedDrink(this.drinkList.get(1));
    }
    //------------------------------------------------------------------------------------------------------------------
    public void showAvailableDrinks(){
        for(int i = 0; i < this.drinkList.size(); i++){
            if(this.drinkList.get(i) == this.selectedDrink) {
                System.out.print(this.drinkList.get(i).getName().toUpperCase());
            }
            else {
            System.out.print(i + "." + this.drinkList.get(i).getName().toLowerCase());
            }
            System.out.print(" | ");
        }
        System.out.println();
    }
    //------------------------------------------------------------------------------------------------------------------
    public void prepareDrink() throws InterruptedException {
        if(selectedDrink != null){
            System.out.println("------- Preparation of your: " + this.selectedDrink.getName().toUpperCase() + " ---------");
            this.prepareDrinkState();
        }
        else System.out.println("No selected coffee!!!");

    }
    //------------------------------------------------------------------------------------------------------------------
    public void prepareDrink(boolean isTwoCups) throws InterruptedException {
        if(selectedDrink != null){
            if(isTwoCups){
                System.out.println("------- Preparation of your " + this.selectedDrink.getName().toUpperCase() + " first cup -------");
                this.prepareDrinkState();
                System.out.println("------- Preparation of your "+ this.selectedDrink.getName().toUpperCase() + " second cup ------");
                this.prepareDrinkState();
            }
        }
        else System.out.println("No selected coffee!!!");

    }
    //------------------------------------------------------------------------------------------------------------------
    private void prepareDrinkState() throws InterruptedException {
        System.out.print("\t\tRunning");
        for(int i = 0; i < 10; i++){
            System.out.print("#");
            Thread.sleep(1000);
        }
        System.out.print(" Done!!!");
        Thread.sleep(3000);
    }
    //------------------------------------------------------------------------------------------------------------------
    public Drink selectDrink(String drinkName){
        for(Drink d: drinkList){
            if(d.getName().equals(drinkName))
                return d;
        }
        return null;
    }
    //------------------------------------------------------------------------------------------------------------------
    public void initDrinks(){
        this.drinkList = new ArrayList<>();
        for (String s : this.drinkNameList) this.drinkList.add(new Drink(s));
    }
    //------------------------------------------------------------------------------------------------------------------
    public MenuOperation getMenuOperation() { return menuOperation; }
    public Drink getSelectedDrink() { return selectedDrink; }
    public void setSelectedDrink(Drink selectedDrink) {
        this.selectedDrink = selectedDrink;
    }
    public boolean isTwoCups() {
        return twoCups;
    }
    public void setTwoCups(boolean twoCups) {
        this.twoCups = twoCups;
    }
    public List<Drink> getDrinkList() {
        return drinkList;
    }

//    public void setDrinkList(ArrayList<Drink> drinkList) {
//        this.drinkList = drinkList;
//    }

}
