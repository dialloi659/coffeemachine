package com.doditech;

import com.doditech.controllers.ControlPanelController;
import com.doditech.models.Intensity;
import com.doditech.models.Quantity;
import com.doditech.views.Screen;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Screen.startScreen();
    }

//    //------------------------------------------------------------------------------------------------------------------
//    private static void selectCoffeeMenu() throws InterruptedException {
////        clearConsole();
//        System.out.println("************************ Select coffee ******************************");
//
//        CONTROL_PANEL_CONTROLLER.showAvailableDrinks();
//        System.out.print("Click number of your choice: ");
//        int choice = new Scanner(System.in).nextInt();
//        switch (choice) {
//            case 0:
//                CONTROL_PANEL_CONTROLLER.setSelectedDrink(CONTROL_PANEL_CONTROLLER.getDrinkList().get(0));
//                break;
//            case 1:
//                CONTROL_PANEL_CONTROLLER.setSelectedDrink(CONTROL_PANEL_CONTROLLER.getDrinkList().get(1));
//                break;
//            case 2:
//                CONTROL_PANEL_CONTROLLER.setSelectedDrink(CONTROL_PANEL_CONTROLLER.getDrinkList().get(2));
//                break;
//            case 3:
//                CONTROL_PANEL_CONTROLLER.setSelectedDrink(CONTROL_PANEL_CONTROLLER.getDrinkList().get(3));
//                break;
//        }
//        showSelectedDrinkDetails();
//        selectedCoffeeSubMenu();
//    }

//    //------------------------------------------------------------------------------------------------------------------
//    private static void selectedCoffeeSubMenu() {
//        boolean quit = false;
//        do {
////            clearConsole();
//            System.out.println("0-SetIntensity || 1-SetQuantity || 2-TwoCups || 8-Start || 9-Back");
//            choiceCoffee = new Scanner(System.in).nextInt();
//
//            switch (choiceCoffee) {
//                case 0:
//                    setIntensity();
//                    quit = false;
//                    break;
//                case 1:
//                    setQuantity();
//                    quit = false;
//                    break;
//                case 2:
//                    CONTROL_PANEL_CONTROLLER.setTwoCups(true);
//                    break;
//                case 8:
//                case 9:
//                    quit = true;
//                    break;
//            }
//        } while (!quit);
//    }
//
//    private static void menu() {
//    }
//
//    private static void favoriteDrinkMenu() {
//    }
//
//    //-------------------------------------------------------------------------------------------------------------------
//    @SuppressWarnings("InfiniteLoopStatement")
//    public static void mainScreen() throws InterruptedException {
////        String showCoffeeSentence = "************************ Select coffee ******************************";
////        String selectedCoffeeDetailsSentence = "____________________Selected coffee details_______________________";
//        do {
//            System.out.println("0-Select coffee || 1-MainMenu || 2.Lock || 3.Favorite Drink");
//            System.out.print("Click number of your choice: ");
//            int choice = new Scanner(System.in).nextInt();
//            switch (choice) {
//                case 0:
//                    selectCoffeeMenu();
//                    if (choiceCoffee == 8) {
//                        if (!CONTROL_PANEL_CONTROLLER.isTwoCups()) {
//                            CONTROL_PANEL_CONTROLLER.prepareDrink();
//                        } else {
//                            CONTROL_PANEL_CONTROLLER.prepareDrink(CONTROL_PANEL_CONTROLLER.isTwoCups());
//                        }
//                    CONTROL_PANEL_CONTROLLER.setTwoCups(false);
//                    }
//                    break;
//                case 1:
//                    menu();
//                    break;
//                case 2:
//                    System.out.println("locked!!!");
//                    break;
//                case 4:
//                    favoriteDrinkMenu();
//                    break;
//            }
//        } while (true);
//    }
//
//
//    //------------------------------------------------------------------------------------------------------------------
//    public static void clearConsole() {
//
//        for (int i = 0; i < 80 * 300; i++) // Default Height of cmd is 300 and Default width is 80
////        for(int i = 0; i < 50; i++) // Default Height of cmd is 300 and Default width is 80
//            System.out.print("\b"); //
//
//    }
//
//    //------------------------------------------------------------------------------------------------------------------
//    private static void showSelectedDrinkDetails() {
//        System.out.println("------" + CONTROL_PANEL_CONTROLLER.getSelectedDrink().getName().toUpperCase() + " details: -------");
//
//        System.out.println("Intensity: " + CONTROL_PANEL_CONTROLLER.getSelectedDrink().getIntensity().name().toUpperCase() +
//                "\t||\t" + CONTROL_PANEL_CONTROLLER.getSelectedDrink().getQuantity().name().toUpperCase() + " :Quantity");
//    }
//
//    //------------------------------------------------------------------------------------------------------------------
//    private static void setQuantity() {
//        int choiceTmp = 10;
//        boolean quit = false;
//        Quantity quantity = null;
//        Quantity[] quantities = Quantity.values();
//        do {
//            for (int i = 0; i < quantities.length; i++) {
//
//                if (choiceTmp == i) {
//                    System.out.print(" | " + i + "-" + quantities[i].name().toUpperCase());
//                } else {
//                    System.out.print(" | " + i + "-" + quantities[i].name().toLowerCase());
//                }
//            }
//            System.out.print(" | 8-Validate");
//            System.out.println(" | 9-Back");
//
//            choiceTmp = new Scanner(System.in).nextInt();
//
//            if (choiceTmp >= 0 && choiceTmp <= quantities.length) {
//                quantity = quantities[choiceTmp];
//            } else if (choiceTmp == 8) {
//                CONTROL_PANEL_CONTROLLER.getSelectedDrink().setQuantity(quantity);
//                System.out.println("Choose validate!");
//                quit = true;
//            } else if (choiceTmp == 9) {
//                System.out.println("Choose back!");
//                quit = true;
//            } else {
//                System.out.println("////////////////////////////////////");
//                System.out.println("Please choose a good selection number!!!");
//                System.out.println("////////////////////////////////////");
//            }
//
//        } while (!quit);
//        showSelectedDrinkDetails();
//    }
//
//    //------------------------------------------------------------------------------------------------------------
//    private static void setIntensity() {
//        int choiceTmp = 10;
//        boolean quit = false;
//        Intensity intensity = null;
//        Intensity[] intensities = Intensity.values();
//        do {
//            for (int i = 0; i < intensities.length; i++) {
//
//                if (choiceTmp == i) {
//                    System.out.print(" | " + i + "-" + intensities[i].name().toUpperCase());
//                } else {
//                    System.out.print(" | " + i + "-" + intensities[i].name().toLowerCase());
//                }
//            }
//            System.out.print(" | 8-Validate");
//            System.out.println(" | 9-Back");
//
//
//            choiceTmp = new Scanner(System.in).nextInt();
//
//            if (choiceTmp >= 0 && choiceTmp <= intensities.length) {
//                intensity = intensities[choiceTmp];
//            } else if (choiceTmp == 8) {
//                System.out.println("Choose validate!");
//                CONTROL_PANEL_CONTROLLER.getSelectedDrink().setIntensity(intensity);
//                quit = true;
//            } else if (choiceTmp == 9) {
//                System.out.println("Choose back!");
//                quit = true;
//            } else {
//                System.out.println("////////////////////////////////////");
//                System.out.println("Please choose a good selection number!!!");
//                System.out.println("////////////////////////////////////");
//            }
//            showSelectedDrinkDetails();
//        } while (!quit);
//    }
}
