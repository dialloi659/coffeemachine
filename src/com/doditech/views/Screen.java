package com.doditech.views;

import com.doditech.controllers.ControlPanelController;
import com.doditech.models.MainMenu;

public class Screen {
   static MainMenu menu ;
    static   {
       menu = new MainMenu();
    }
    public static void startScreen() throws InterruptedException {
        menu.mainScreen();
    }
}
