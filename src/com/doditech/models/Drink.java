package com.doditech.models;

public class Drink {
    private String name;
    private Intensity intensity;
    private Quantity quantity;

    public Drink(String name) {
        this.name = name;
        this.intensity = Intensity.NORMAL;
        this.quantity = Quantity.AVERAGE;
    }

    @Override
    public String toString() {
        return "------" + this.getName().toUpperCase() + " details: -------\n" +
                "Intensity: " + this.getIntensity().name().toUpperCase() +
                "\t||\t" + this.getQuantity().name().toUpperCase() + " :Quantity";
    }

    public Drink(String name, Intensity intensity, Quantity quantity) {
        this(name);
        this.intensity = intensity;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Intensity getIntensity() {
        return intensity;
    }

    public void setIntensity(Intensity intensity) {
        this.intensity = intensity;
    }

    public Quantity getQuantity() {
        return quantity;
    }

    public void setQuantity(Quantity quantity) {
        this.quantity = quantity;
    }
}
