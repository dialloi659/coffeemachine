package com.doditech.models;

public enum Quantity {
    SMALL,
    AVERAGE,
    BIG
}
