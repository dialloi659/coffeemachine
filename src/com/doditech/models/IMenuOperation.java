package com.doditech.models;

public interface IMenuOperation {
    void cleanMachine() throws InterruptedException;
    void diagnostic() throws InterruptedException;
}
