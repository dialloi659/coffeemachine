package com.doditech.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MenuOperation implements IMenuOperation {
    private static final String[] operationsList = new String[]{"Clean machine", "Diagnostic"};

    public String[] getOperationsList() {
        return operationsList;
    }

    @Override
    public void cleanMachine() throws InterruptedException {
        System.out.println("---------- Cleaning machine started!!! ------------");
        System.out.print("\t\tRunning ");
        for(int i = 0; i < 5; i++){
            System.out.print("#");
            Thread.sleep(1000);
        }
        System.out.println(" Done!!!");
        Thread.sleep(2000);
    }

    @Override
    public void diagnostic() throws InterruptedException {
        System.out.println("---------- Diagnostic program started!!! ------------");
        System.out.print("\t\tRunning ");
        for(int i = 0; i < 5; i++){
            System.out.print("#");
            Thread.sleep(1000);
        }
        System.out.println(" Done!!!");
        Thread.sleep(2000);
    }

    @Override
    public String toString() {
        return "----------------Choose Menu Operation------------------";
    }
}
