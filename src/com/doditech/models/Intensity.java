package com.doditech.models;

public enum Intensity {
    STRONG,
    NORMAL,
    SOFT
}
