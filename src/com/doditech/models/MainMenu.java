package com.doditech.models;

import com.doditech.controllers.ControlPanelController;

import java.util.Scanner;

public class MainMenu {
    private final ControlPanelController pannelContoller ;
    private int startStopTempMemory;
    private boolean isLocked;

    public MainMenu() {
        this.pannelContoller = new ControlPanelController();
        this.startStopTempMemory = 100;
        this.isLocked = false;
    }
    //------------------------------------------------------------------------------------------------------------------
    private void selectCoffeeMenu() {
        boolean isQuit = false;
        do{
            System.out.println("************************ Select coffee ******************************");

            pannelContoller.showAvailableDrinks();

            System.out.println(" | 8.Validate | 9.Back");
            System.out.print("Click number of your choice: ");
            int choice = new Scanner(System.in).nextInt();
            switch (choice) {
                case 0:
                    pannelContoller.setSelectedDrink(pannelContoller.getDrinkList().get(0));
                    break;
                case 1:
                    pannelContoller.setSelectedDrink(pannelContoller.getDrinkList().get(1));
                    break;
                case 2:
                    pannelContoller.setSelectedDrink(pannelContoller.getDrinkList().get(2));
                    break;
                case 3:
                    pannelContoller.setSelectedDrink(pannelContoller.getDrinkList().get(3));
                    break;
                case 9:
                case 8:
                    selectedCoffeeSubMenu();
                    isQuit = true;
                    break;
            }
//        showSelectedDrinkDetails();
        } while (!isQuit);

    }
    //------------------------------------------------------------------------------------------------------------------
    private void selectedCoffeeSubMenu() {
        boolean quit = false;
        do {
            System.out.println("0-SetIntensity || 1-SetQuantity || 2-TwoCups || 8-Start || 9-Back");
            this.startStopTempMemory = new Scanner(System.in).nextInt();

            switch (startStopTempMemory) {
                case 0:
                    setIntensity();
                    System.out.println(this.pannelContoller.getSelectedDrink().toString());
//                    quit = false;
                    break;
                case 1:
                    setQuantity();
                    System.out.println(this.pannelContoller.getSelectedDrink().toString());
                    break;
                case 2:
                    pannelContoller.setTwoCups(true);
                    break;
                case 8:
                case 9: quit = true;
            }
        } while (!quit);
    }
    //-------------------------------------------------------------------------------------------------------------------
    @SuppressWarnings("InfiniteLoopStatement")
    public void mainScreen() throws InterruptedException {
        do {
            System.out.println();
            if(!isLocked){
                System.out.println("0-Select coffee || 1-Menu || 2.Lock || 3.Favorite Drink");
            }
            else {
                System.out.println("Click 2 to unlock please!");
            }
            System.out.print("Click number of choice: ");
            int choice = new Scanner(System.in).nextInt();
            switch (choice) {
                case 0:
                    selectCoffeeMenu();
                    if (startStopTempMemory == this.pannelContoller.validate) {
                        if (!pannelContoller.isTwoCups()) {
                            pannelContoller.prepareDrink();
                        } else {
                            pannelContoller.prepareDrink(pannelContoller.isTwoCups());
                        }
                        pannelContoller.setTwoCups(false);
                    }
                    break;
                case 1:
                    menuMenu();
                    break;
                case 2:
                    System.out.println("System locked!!!");
                    this.isLocked = !this.isLocked;
                    break;
                case 4:
//                    favoriteDrinkMenu();
                    break;
            }
        } while (true);
    }
    private void menuMenu() throws InterruptedException {
        boolean quit = false;
        int temp = 10;
        int choice = 10; //juste to init
        do{
            System.out.println(this.pannelContoller.getMenuOperation().toString());
            for(int i = 0; i < this.pannelContoller.getMenuOperation().getOperationsList().length; i++){
                if(i == choice)
                    System.out.println(i + "." + this.pannelContoller.getMenuOperation().getOperationsList()[i].toUpperCase() + " || ");
                else
                    System.out.println(i + "." + this.pannelContoller.getMenuOperation().getOperationsList()[i].toLowerCase() + " || ");
            }
            choice = new Scanner(System.in).nextInt();
            switch (choice){
                case 0: temp = 0; break;
                case 1: temp = 1; break;
                case 8:
                case 9: quit = true; break;
                default:
            }
        }while(!quit);
        if(temp == 0) this.pannelContoller.getMenuOperation().cleanMachine();
        else if (temp == 1) this.pannelContoller.getMenuOperation().diagnostic();
    }
    //------------------------------------------------------------------------------------------------------------------
    public void clearConsole() {

        for (int i = 0; i < 80 * 300; i++) // Default Height of cmd is 300 and Default width is 80
            System.out.print("\b"); //

    }
//    //------------------------------------------------------------------------------------------------------------------
//    private void showSelectedDrinkDetails() {
//        System.out.println("------" + pannelContoller.getSelectedDrink().getName().toUpperCase() + " details: -------");
//
//        System.out.println("Intensity: " + pannelContoller.getSelectedDrink().getIntensity().name().toUpperCase() +
//                "\t||\t" + pannelContoller.getSelectedDrink().getQuantity().name().toUpperCase() + " :Quantity");
//    }
    //------------------------------------------------------------------------------------------------------------------
    private void setQuantity() {
        int choiceTmp = 10;
        boolean quit = false;
        Quantity quantity = null;
        Quantity[] quantities = Quantity.values();
        do {
            for (int i = 0; i < quantities.length; i++) {

                if (choiceTmp == i) {
                    System.out.print(" | " + i + "-" + quantities[i].name().toUpperCase());
                } else {
                    System.out.print(" | " + i + "-" + quantities[i].name().toLowerCase());
                }
            }
            System.out.print(" | 8-Validate");
            System.out.println(" | 9-Back");

            choiceTmp = new Scanner(System.in).nextInt();

            if (choiceTmp >= 0 && choiceTmp <= quantities.length) {
                quantity = quantities[choiceTmp];
            } else if (choiceTmp == 8) {
                pannelContoller.getSelectedDrink().setQuantity(quantity);
                System.out.println("Choose validate!");
                quit = true;
            } else if (choiceTmp == 9) {
                System.out.println("Choose back!");
                quit = true;
            } else {
                System.out.println("////////////////////////////////////");
                System.out.println("Please choose a good selection number!!!");
                System.out.println("////////////////////////////////////");
            }

        } while (!quit);
//        showSelectedDrinkDetails();
    }
    //------------------------------------------------------------------------------------------------------------
    private void setIntensity() {
        int choiceTmp = 10;
        boolean quit = false;
        Intensity intensity = null;
        Intensity[] intensities = Intensity.values();
        do {
            for (int i = 0; i < intensities.length; i++) {

                if (choiceTmp == i) {
                    System.out.print(" | " + i + "-" + intensities[i].name().toUpperCase());
                } else {
                    System.out.print(" | " + i + "-" + intensities[i].name().toLowerCase());
                }
            }
            System.out.print(" | 8-Validate");
            System.out.println(" | 9-Back");

            choiceTmp = new Scanner(System.in).nextInt();

            if (choiceTmp >= 0 && choiceTmp <= intensities.length) {
                intensity = intensities[choiceTmp];
            } else if (choiceTmp == 8) {
                System.out.println("Choose validate!");
                pannelContoller.getSelectedDrink().setIntensity(intensity);
                quit = true;
            } else if (choiceTmp == 9) {
                System.out.println("Choose back!");
                quit = true;
            } else {
                System.out.println("////////////////////////////////////");
                System.out.println("Please choose a good selection number!!!");
                System.out.println("////////////////////////////////////");
            }
//            showSelectedDrinkDetails();
        } while (!quit);
    }
}
